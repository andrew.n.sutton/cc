#pragma once

#include <cc/file.hpp>
#include <cc/diagnostics.hpp>

#include <list>

namespace cc
{
  /// Stores global information about the input for translation.
  class input_manager
  {
    // Use a list to ensure that references are not invalidated.
    using file_seq = std::list<file>;
  public:
    /// Add a new input file.
    const file& add_file(const std::string& path);

    /// Add text to be interpreted as input.
    const file& add_text(const std::string& str);

    /// Returns the file in which a source code location resides.
    const file& get_file(location loc) const;

    /// Returns a resolved and 1-based line and column numbers for the 
    /// location.
    full_location resolve_location(location loc) const;

    // File iteration
    using file_iterator = file_seq::const_iterator;
    
    file_iterator begin_files() const { return input.begin(); }
    file_iterator end_files() const { return input.end(); }

  private:
    std::size_t current_offset() const;

    /// The input buffers for translation.
    file_seq input;
  };


  /// Represents an input file error.
  class input_error : public diagnosable_error
  {
  public:
    input_error(const std::string& msg)
      : diagnosable_error(dk_error, "input", {}, msg)
    { }
  };

} // namespace cc
