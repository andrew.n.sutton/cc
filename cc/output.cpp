#include "output.hpp"

#include <cassert>
#include <iostream>
#include <array>

#include <unistd.h>

namespace cc
{
  // TODO: This is not guaranteed to be accurate. 
  static bool 
  detect_terminal(std::ostream& os)
  {
    if (&os == &std::cout)
      return isatty(STDOUT_FILENO);
    else if (&os == &std::cerr)
      return isatty(STDERR_FILENO);
    else if (&os == &std::clog)
      return isatty(STDERR_FILENO);    
    else
      return false;
  }

  output_device::output_device(std::ostream& os)
    : os(os), term(detect_terminal(os)), color(term)
  { }

// -------------------------------------------------------------------------- //
// Output fonts

  /// A list of mode strings computed from a font specification.
  using mode_list = std::array<const char*, 8>;

  static void
  get_weight(mode_list& modes, int& n, fontspec fs)
  {
    assert(n < modes.size());
    switch (fs.get_intensity()) {
    case font_intensity::bold:
      modes[n++] = "1";
      break;
    case font_intensity::faint:
      modes[n++] = "2";
      break;
    default:
      break;
    }
  }

  static void
  get_underline(mode_list& modes, int& n, fontspec fs)
  {
    switch (fs.get_underline()) {
    case font_flag::on:
      modes[n++] = "3";
      break;
    case font_flag::off:
      modes[n++] = "24";
      break;
    default:
      break;
    }
  }

  // FIXME: There must be a more elegant way of doing this. Maybe make each
  // mode string a 4-character buffer and write directly into that?
  static void
  get_fg_color(mode_list& modes, int& n, fontspec fs)
  {
    switch (fs.get_color()) {
    case font_color::black:
      modes[n++] = "30"; 
      break;
    case font_color::red:
      modes[n++] = "31"; 
      break;
    case font_color::green:
      modes[n++] = "32"; 
      break;
    case font_color::yellow:
      modes[n++] = "33"; 
      break;
    case font_color::blue:
      modes[n++] = "34"; 
      break;
    case font_color::magenta:
      modes[n++] = "35"; 
      break;
    case font_color::cyan:
      modes[n++] = "36"; 
      break;
    case font_color::white:
      modes[n++] = "37"; 
      break;
    case font_color::normal:
      modes[n++] = "39"; 
      break;
    case font_color::number:
    case font_color::rgb:
      throw std::logic_error("not implemented");
    case font_color::unset:
      break;
    }
  }

  static void
  get_bg_color(mode_list& modes, int& n, fontspec fs)
  {
    switch (fs.get_background()) {
    case font_color::black:
      modes[n++] = "40"; 
      break;
    case font_color::red:
      modes[n++] = "41"; 
      break;
    case font_color::green:
      modes[n++] = "42"; 
      break;
    case font_color::yellow:
      modes[n++] = "43"; 
      break;
    case font_color::blue:
      modes[n++] = "44"; 
      break;
    case font_color::magenta:
      modes[n++] = "45"; 
      break;
    case font_color::cyan:
      modes[n++] = "46"; 
      break;
    case font_color::white:
      modes[n++] = "47"; 
      break;
    case font_color::normal:
      modes[n++] = "49"; 
      break;
    case font_color::number:
    case font_color::rgb:
      throw std::logic_error("not implemented");
    case font_color::unset:
      break;
    }
  }

  static void 
  get_modes(mode_list& modes, fontspec fs)
  {
    int n = 0;

    // If this is the default font, don't process any more.
    if (fs.is_default()) {
      modes[n++] = "0";
      return;
    }

    get_weight(modes, n, fs);
    get_underline(modes, n, fs);
    get_fg_color(modes, n, fs);
    get_bg_color(modes, n, fs);
  }

  std::ostream& operator<<(std::ostream& os, fontspec fs)
  {
    mode_list modes{};
    get_modes(modes, fs);

    // If there's at least one mode flag (there always should be...),
    // write out the list of modifiers.
    if (*modes[0]) {
      os << "\033[";
      for (auto iter = modes.begin(); iter != modes.end() && *iter; ++iter) {
        os << *iter;
        if (std::next(iter) != modes.end() && *std::next(iter))
          os << ';';
      }
      os << 'm';
    }

    return os;
  }

} // namespace cc
