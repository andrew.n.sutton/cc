#include "bytecode.hpp"

#include <cassert>
#include <cstring>

namespace cc
{
// -------------------------------------------------------------------------- //
// Output

  static void
  append_bytes(byte_seq& bytes, void* p, std::size_t n)
  {
    const byte* first = reinterpret_cast<const byte*>(p);
    const byte* limit = first + n;
    bytes.insert(bytes.end(), first, limit);
  }

  template<typename T>
  static void 
  append_scalar(byte_seq& bytes, T n)
  {
    append_bytes(bytes, &n, sizeof(T));
  }

  void
  byte_ostream::write_bytes(const byte_seq& bs)
  {
    bytes.insert(bytes.end(), bs.begin(), bs.end());
  }

  void
  byte_ostream::write_bool(bool b)
  {
    bytes.push_back(b);
  }

  /// Integer values are encoded based on their value, not their type. Values
  /// less than 127 are stored in a single byte. This should be (by far) the
  /// most common integer encoding.
  ///
  /// Values greater than 127 are stored in 2^k bytes where 2^k is the
  /// number of bytes needed to store the value. A header byte prefixes
  /// the integer storage. The high-order bit of the header is set, and
  /// the remaining 7 values encode k. 
  ///
  /// For example, the value 240 requires 1 byte and is encoded as.
  ///
  ///    80 f0
  ///
  /// The 0x80 is the header with the high order bit set, and k = 0. That
  /// is 2^0 = 1 byte of trailing storage.
  ///
  /// The value 2^16 -1 requries 2 bytes and is encoded as 
  ///
  ///    81 ff ff
  ///
  /// The value 2^16 requires 4 bytes and is encoded as
  ///
  ///    82 00 01 00 00
  ///
  /// NOTE: There is a lot of extra room available in the header of
  /// integers. We could potentially repurpose that for other uses.
  void
  byte_ostream::write_int(std::uint64_t n)
  {
    if (n < (1 << 7)) {
      write_int8(n);
    }
    else if (n < (1 << 8)) {
      write_int8(0x80 | 0);
      write_int8(n);
    }
    else if (n < (1 << 16)) {
      write_int8(0x80 | 1);
      write_int16(n);
    }
    else if (n < (1ll << 32)) {
      write_int8(0x80 | 2);
      write_int32(n);
    }
    else {
      write_int8(0x80 | 3);
      write_int64(n);      
    }
  }

  void
  byte_ostream::write_int8(std::uint8_t n)
  {
    bytes.push_back(n);
  }

  void
  byte_ostream::write_int16(std::uint16_t n)
  {
    append_scalar(bytes, n);
  }

  void
  byte_ostream::write_int32(std::uint32_t n)
  {
    append_scalar(bytes, n);
  }

  void
  byte_ostream::write_int64(std::uint64_t n)
  {
    append_scalar(bytes, n);
  }

  void
  byte_ostream::write_string(const char* str, std::size_t len)
  {
    write_int(len);
    bytes.insert(bytes.end(), str, str + len);
  }

  void
  byte_ostream::write_string(const std::string& str)
  {
    write_string(str.c_str(), str.size());
  }

// -------------------------------------------------------------------------- //
// Input

  template<typename T>
  void
  read_scalar(const byte_seq& bytes, std::size_t& pos, T& n)
  {
    assert((pos + sizeof(T) < bytes.size()) && "stream overflow");
    std::memcpy(&n, &bytes[pos], sizeof(T));
    pos += sizeof(T);
  }

  void
  byte_istream::read_bytes(byte_seq& buf)
  {
    assert((buf.size() <= bytes.size() - pos) && "stream overflow");
    const byte* first = bytes.data() + pos;
    const byte* limit = first + buf.size();
    std::copy(first, limit, buf.begin());
    pos += buf.size();
  }

  bool
  byte_istream::read_bool()
  {
    assert(pos < bytes.size() && "stream overflow");
    return bytes[pos++];
  }

  std::uint64_t
  byte_istream::read_int()
  {
    assert(pos < bytes.size() && "stream overflow");
    std::uint8_t n = bytes[pos++];
    
    // Check for small integers.
    if (!(n & 0x80))
      return n;

    // Decode a larger integer value.
    switch (n & 0x7f) {
      case 0:
        return read_int8();
      case 1:
        return read_int16();
      case 2:
        return read_int32();
      case 3:
        return read_int64();
      default:
        // FIXME: Support 128-bit integers? Implement a dynamic
        // integer loader?
        assert(false && "invalid integer size");
    }
  }

  std::uint8_t
  byte_istream::read_int8()
  {
    assert(pos < bytes.size() && "stream overflow");
    return bytes[pos++];
  }

  std::uint16_t
  byte_istream::read_int16()
  {
    std::uint16_t ret;
    read_scalar(bytes, pos, ret);
    return ret;
  }

  std::uint32_t
  byte_istream::read_int32()
  {
    std::uint32_t ret;
    read_scalar(bytes, pos, ret);
    return ret;
  }

  std::uint64_t
  byte_istream::read_int64()
  {
    std::uint64_t ret;
    read_scalar(bytes, pos, ret);
    return ret;
  }

  std::string
  byte_istream::read_string()
  {
    std::size_t len = read_int();
    assert((pos + len) <= bytes.size() && "stream overflow");
    std::string ret(&bytes[pos], &bytes[pos + len]);
    pos += len;
    return ret;
  }

} // namespace cc
