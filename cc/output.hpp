#pragma once

#include <iosfwd>
#include <utility>

namespace cc
{
  /// Represents information about an outpout stream.
  class output_device
  {
  public:
    output_device(std::ostream& os);

    /// Returns the output stream.
    std::ostream& get_stream() const { return os; }

    /// Returns true if the stream is attached to a terminal.
    bool is_terminal() const { return term; }

    /// Returns true if output supports color. Color output is supported
    /// by default when the stream is attached to a terminal.
    bool use_color() const { return color; }

    /// Explicitly enable or disable color. If the output is not a terminal,
    /// or if the outpout format is not plain text, escape codes may cause
    /// rendering issues.
    void enable_color(bool b) { color = b; }

  protected:
    /// The underlying output stream.
    std::ostream& os;

    /// True if emitting to a terminal.
    bool term;
    
    /// True the output device supports
    bool color;
  };


// -------------------------------------------------------------------------- //
// Output fonts

  /// A tri-state flag.
  enum class font_flag
  {
    unset, // The flag was not set.
    on,
    off,
  };

  /// Font intensity (weight).
  enum class font_intensity
  {
    unset, // The weight was not specified.
    bold,
    faint
  };

  /// Foreground, background color.
  enum class font_color
  {
    unset, // The color was not specified.
    black,
    red,
    green,
    yellow,
    blue,
    magenta,
    cyan,
    white,
    normal, // The default font color.
    number, // The color is determined by an index (0-255)
    rgb, // The color is determined by an rgb triple.
  };

  /// Represents a specification for a font mode. When written to an output
  /// stream the fontspec will modify the current font in the terminal (if
  /// supported). This means that fonts can selectively enable or disable
  /// particular aspects (e.g., changing from underline to non-underline).
  ///
  /// \todo Support indexed and rgb font colors.
  struct fontspec
  {
    // Creates a request for the default font.
    fontspec()
      : _default(true), 
        _intensity(),
        _italic(),
        _underline(),
        _overline(),
        _strikethrough(),
        _foreground(),
        _background()
    { }

    /// Returns true if this is the default font.
    bool is_default() const { return _default; }

    /// Returns the font intensity.
    font_intensity get_intensity() const { return (font_intensity)_intensity; }

    /// Returns the italic flag.
    font_flag get_italic() const { return (font_flag)_italic; }

    /// Returns the italic flag.
    font_flag get_underline() const { return (font_flag)_underline; }

    /// Returns the italic flag.
    font_flag get_overline() const { return (font_flag)_overline; }

    /// Returns the italic flag.
    font_flag get_strikethrough() const { return (font_flag)_strikethrough; }

    /// Returns the font color.
    font_color get_color() const { return (font_color)_foreground; }

    /// Returns the font background color.
    font_color get_background() const { return (font_color)_background; }

    // Font builder

    // Intensity

    /// Returns a copy of the font with the specified intensity.
    fontspec with_intensity(font_intensity w) const
    {
      fontspec fs = *this;
      fs._default = false;
      fs._intensity = (unsigned)w;
      return fs;
    }

    /// Returns a bold copy of the font.
    fontspec bold() const { return with_intensity(font_intensity::bold); }

    /// Returns a faint copy of the font.
    fontspec faint() const { return with_intensity(font_intensity::faint); }

    // Italic

    /// Returns a copy of the font with the italic flag set.
    fontspec with_italic(font_flag f) const
    {
      fontspec fs = *this;
      fs._default = false;
      fs._italic = (unsigned)f;
      return fs;
    }

    /// Returns an italic copy of the font.
    fontspec italic() const { return with_italic(font_flag::on); }
    
    /// Returns an upright copy of the font.
    fontspec upright() const { return with_italic(font_flag::off); }

    // Underline

    /// Returns a copy of the font with the underline flag set.
    fontspec with_underline(font_flag f) const
    {
      fontspec fs = *this;
      fs._default = false;
      fs._underline = (unsigned)f;
      return fs;
    }

    /// Returns an underlined copy of the font.
    fontspec underline() const { return with_underline(font_flag::on); }
    
    /// Returns an non-underlined copy of the font.
    fontspec no_underline() const { return with_underline(font_flag::off); }

    // Overline

    /// Returns a copy of the font with the overline flag set.
    fontspec with_overline(font_flag f) const
    {
      fontspec fs = *this;
      fs._default = false;
      fs._overline = (unsigned)f;
      return fs;
    }

    /// Returns an overlined copy of the font.
    fontspec overline() const { return with_overline(font_flag::on); }
    
    /// Returns an non-overlined copy of the font.
    fontspec no_overline() const { return with_overline(font_flag::off); }

    // Strikethrough

    /// Returns a copy of the font with the strikethrough flag set.
    fontspec with_strikethrough(font_flag f) const
    {
      fontspec fs = *this;
      fs._default = false;
      fs._strikethrough = (unsigned)f;
      return fs;
    }

    /// Returns an strikethrough copy of the font.
    fontspec strikethrough() const { return with_strikethrough(font_flag::on); }
    
    /// Returns an non-strikethrough copy of the font.
    fontspec no_strikethrough() const { return with_strikethrough(font_flag::off); }

    // Color

    /// Returns a copy of the font with the specified foreground color.
    fontspec with_color(font_color c) const
    {
      fontspec fs = *this;
      fs._default = false;
      fs._foreground = (unsigned)c;
      return fs;
    }

    fontspec black() const { return with_color(font_color::black); }
    fontspec red() const { return with_color(font_color::red); }
    fontspec green() const { return with_color(font_color::green); }
    fontspec blue() const { return with_color(font_color::blue); }
    fontspec cyan() const { return with_color(font_color::cyan); }
    fontspec magenta() const { return with_color(font_color::magenta); }
    fontspec yellow() const { return with_color(font_color::yellow); }
    fontspec white() const { return with_color(font_color::white); }

    /// Returns a copy of the font with the specified background color.
    fontspec with_background(font_color c) const
    {
      fontspec fs = *this;
      fs._default = false;
      fs._background = (unsigned)c;
      return fs;
    }

    fontspec on_black() const { return with_background(font_color::black); }
    fontspec on_red() const { return with_background(font_color::red); }
    fontspec on_green() const { return with_background(font_color::green); }
    fontspec on_blue() const { return with_background(font_color::blue); }
    fontspec on_cyan() const { return with_background(font_color::cyan); }
    fontspec on_magenta() const { return with_background(font_color::magenta); }
    fontspec on_yellow() const { return with_background(font_color::yellow); }
    fontspec on_white() const { return with_background(font_color::white); }

    /// Enclosed printing
    template<typename T>
    struct typeset_term;

    /// Returns an object that can be written to an ostream with the
    /// font selected by this object.
    template<typename T>
    typeset_term<T> operator()(T&& t) const;


    /// Normal or default font. If set, ignore all other flags.
    unsigned _default : 1; 

    /// Bold/faint. See font_intensity
    unsigned _intensity : 2;

    /// Whether italic.
    unsigned _italic : 2;

    /// Whether underlined.
    unsigned _underline : 2;

    /// Whether overlined.
    unsigned _overline : 2;

    /// Whether strikethrough.
    unsigned _strikethrough : 2;

    /// Foreground color. See font_color.
    /// FIXME: This could probably be a variant of some kind.
    unsigned _foreground : 8;

    /// Background font color. See font_color.
    /// FIXME: This could probably be a variant of some kind.
    unsigned _background : 8;
  };

  template<typename T>
  struct fontspec::typeset_term
  {
    typeset_term(fontspec fs, T&& t)
      : value(std::forward<T>(t)), font(fs), norm()
    { }

    T value;
    fontspec font;
    fontspec norm;
  };

  template<typename T>
  fontspec::typeset_term<T> 
  fontspec::operator()(T&& t) const
  {
    return typeset_term<T>(*this, std::forward<T>(t));
  }

  template<typename C, typename T, typename U>
  std::basic_ostream<C, T>& 
  operator<<(std::basic_ostream<C, T>& os, const fontspec::typeset_term<U>& t)
  {
    return os << t.font << t.value << t.norm;
  }

  std::ostream& operator<<(std::ostream& os, fontspec fs);

} // namespace cc
