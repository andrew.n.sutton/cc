#include "input.hpp"
#include "diagnostics.hpp"

#include <cassert>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>

namespace cc
{
  // Compute the starting offset for the new file. Adjust the offset by
  // one so that we can detect ends-of-file.
  std::size_t
  input_manager::current_offset() const
  {
    return input.empty() ? 0 : input.back().get_top_offset();
  }

  const file&
  input_manager::add_file(const std::string& path)
  {
    // Read the file.
    std::ifstream ifs(path);
    if (!ifs) {
      std::stringstream ss;
      ss << "cannot open '" << path << "' for reading";
      throw input_error(ss.str());
    }

    // Add the file.
    input.emplace_back(current_offset(), path, ifs);
    return input.back();
  }

  const file&
  input_manager::add_text(const std::string& str)
  {
    // Add the file.
    input.emplace_back(current_offset(), "<input>", str);
    return input.back();
  }

  const file&
  input_manager::get_file(location loc) const
  {
    assert(loc.is_valid() && "invalid location");
    auto iter = std::find_if(input.begin(), input.end(), [loc](const file& f) {
      return loc.get_index() < f.get_top_offset();
    });
    assert(iter != input.end() && "location outside of input");
    return *iter;
  }

  full_location
  input_manager::resolve_location(location loc) const
  {
    const file& f = get_file(loc);
    coordinates coords = f.resolve_location(loc);
    return {f, coords};
  }

} // namespace cc

