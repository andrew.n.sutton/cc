#pragma once

#include <cc/location.hpp>

#include <iosfwd>
#include <stdexcept>
#include <string>
#include <vector>

namespace cc
{
  /// Kinds of diagnostic ionformation produced during translation.
  enum diagnostic_kind {
    /// There is an unrecoverable error in this stage of translation.
    /// Examples are syntax, type, and interpretation errors.
    dk_error,

    /// A phrase in the program is technically correct as written, but 
    /// has been known to be incorrect in practice (i.e., a likely bug).
    dk_warn,

    /// Information about translation that is neither an error nor a warning.
    /// These can be used for debugging within the compiler.
    dk_info,

    /// Additional information that accompanies a diagnostic.
    dk_note, 
  };

  class diagnostic;

  /// A sequence of diagnostic objects.
  using diagnostic_seq = std::vector<diagnostic>;

  /// A message containing diagnostic information. Diagnostic objects are 
  /// trees. The root node is typically an error or warning and child nodes 
  /// are notes containing additional information.
  ///
  /// In some cases, an error may contain additional errors. For example,
  /// an overload resolution error is emitted at the call site, but a complete
  /// diagnosis of failures will include errors for each invalid candidate.
  ///
  /// \todo Accept a string formatting tree instead of plain string. By
  /// deferring formatting, we can better generate diagnostics as data.
  class diagnostic
  {
  public:
    diagnostic(diagnostic_kind k, location loc, const std::string& str)
      : dk(k), algo(), loc(loc), msg(str)
    { }

    diagnostic(diagnostic_kind k, const char* algo, location loc, const std::string& str)
      : dk(k), algo(algo), loc(loc), msg(str)
    { }

    /// Returns the diagnostic kind.
    diagnostic_kind get_kind() const { return dk; }

    /// Returns true if the diagnostic is an error.
    bool is_error() const { return dk == dk_error; }

    /// Returns true if the diagnostic is a warning.
    bool is_warning() const { return dk == dk_warn; }

    /// Returns true if the diagnostic is a note.
    bool is_note() const { return dk == dk_note; }
        
    /// Returns a string describing the diagnostic kind.
    const char* get_label() const;

    /// Returns a (possibly null) string describing the algorithm. This
    /// modifies the label (e.g., "syntax error").
    const char* get_algorithm() const { return algo; }

    /// Returns the location of the diagnostic.
    location get_location() const { return loc; }

    /// Returns the diagnostic message.
    const std::string& get_message() const { return msg; }

    /// Attach a simple note to an error or warning.
    void note(location loc, const std::string& str);

    /// Returns the notes for a diagnostic.
    const std::vector<diagnostic>& get_notes() const { return notes; }

  private:
    /// The kind of diagnostic.
    diagnostic_kind dk;

    /// A term describing the algorithm from which a diagnostic originated.
    /// Ths can be null.
    const char* algo;

    /// The location of the diagnostic.
    location loc;

    /// The fully rendered message.
    std::string msg;

    /// A list of additional (structured) diagnostics.
    diagnostic_seq notes;
  };

  /// An error diagnostic.
  struct error : diagnostic
  {
    error(location loc, const std::string& str)
      : diagnostic(dk_error, loc, str)
    { }

    error(const char* algo, location loc, const std::string& str)
      : diagnostic(dk_error, algo, loc, str)
    { }
  };

  /// A warning diagnostic.
  struct warning : diagnostic
  {
    warning(location loc, const std::string& str)
      : diagnostic(dk_warn, loc, str)
    { }

    warning(const char* algo, location loc, const std::string& str)
      : diagnostic(dk_warn, algo, loc, str)
    { }
  };

  /// A note diagnostic.
  struct note : diagnostic
  {
    note(location loc, const std::string& str)
      : diagnostic(dk_note, loc, str)
    { }

    note(const char* algo, location loc, const std::string& str)
      : diagnostic(dk_note, algo, loc, str)
    { }
  };

  /// Manages information about diagnostics.
  class diagnostic_manager
  {
  public:
    diagnostic_manager()
      : num_errs(0)
    { }

    /// Returns true if any errors have been recorded.
    bool has_errors() const { return error_count() != 0; }

    //// Returns the number of errors.
    std::size_t error_count() const { return num_errs; }
        
    /// Returns the current set of diagnostics.
    const diagnostic_seq& get_diagnostics() const { return diags; }    
    
    /// Emit a top-level diagnostic. Notes are always attached to other
    /// diagnostics.
    void emit(const diagnostic& diag);

    /// Emit an error.
    void error(location loc, const std::string& msg);

    /// Emit an algorithm-specific error.
    void error(const char* algo, location loc, const std::string& msg);

    /// Emit a warning.
    void warn(location loc, const std::string& msg);

    /// Emit an algoirthm-specific warning.
    void warn(const char* algo, location loc, const std::string& msg);

    /// Emit compiler information.
    void inform(location loc, const std::string& msg);
    
    /// Emit compiler information about an algorithm.
    void inform(const char* algo, location loc, const std::string& msg);

  private:
    /// Accumulated diagnostics.
    diagnostic_seq diags;

    /// Number of accumuated errors.
    int num_errs;
  };

// -------------------------------------------------------------------------- //
// Serialization

  class input_manager;
  class output_device;

  /// Print a diagnostic to an output stream.
  ///
  /// \todo Allow diagnostics to be rendered into different forms like
  /// JSON or XML. This could be as simple as just adding function such
  /// as print_json or print_xml.
  void print(const diagnostic& diag, const input_manager& in, output_device& os);

  /// Print all diagnostics to the given device.
  void print(const diagnostic_seq& diags, const input_manager& in, output_device& os);
  
// -------------------------------------------------------------------------- //
// Exceptions

  /// An error that occurs during translation. This is designed to be 
  /// (trivially) subclassed to define particular kinds of runtime errors.
  class diagnosable_error : public std::runtime_error, public diagnostic
  {
  public:
    diagnosable_error(diagnostic_kind k, 
                      const char* algo, 
                      location loc, 
                      const std::string& str)
      : std::runtime_error(str), diagnostic(k, algo, loc, str)
    { }
  };

} // namespace cc
