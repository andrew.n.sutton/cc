#include "dump.hpp"

#include <iostream>

namespace cc
{
  void
  dumper::print_indentation()
  {
    os << std::string(nesting * 2, ' ');
  }

  dumper::dump_guard::dump_guard(dumper& d, const node* n, const char* str, bool nl)
  : d(d), nl(nl)
  {
    std::ostream& os = d.get_stream();
    
    d.print_indentation();
    
    // Print the node in green.
    os << "\033[1;32m";
    d.print(str);
    os << "\033[0m";
    d.print(' ');
    
    // Print the address in faint black.
    os << "\033[2;30m";
    d.print(n);
    os << "\033[0m";
  }

  dumper::dump_guard::~dump_guard()
  {
    if (nl)
      d.print_newline();
  }

} // namespace cc
