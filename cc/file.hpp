#pragma once

#include <cc/location.hpp>

#include <map>
#include <string>
#include <vector>

namespace cc
{
  /// An association of source locations to line numbers. Note that this map
  /// uses a 0-based index for line numbers.
  using line_map = std::map<location, unsigned>;

  /// A list of offsets indicating the starts and ends of lines.
  using line_seq = std::vector<std::pair<unsigned, unsigned>>;

  /// An input file.
  ///
  /// This is a glorified string used for lexing. It owns the text to be
  /// translated. It also contains an offset value, which is used to compute
  /// source code locations.
  class file
  {
  public:
    /// Construct a file from the input stream with path p.
    file(std::size_t n, const std::string& p, std::istream& is);
    
    /// Construct a file from a string.
    file(std::size_t n, const std::string& p, const std::string& s);

    /// Returns the text of the fil.
    const std::string& get_text() const { return text; }

    /// Returns the base offset of the file.
    const std::size_t get_base_offset() const { return offset; }

    /// Returns the top offset of the file. The top offset includes
    /// an extra value to detect the end-of-file.
    const std::size_t get_top_offset() const { return offset + get_size() + 1; }

    /// Returns the path of the input file.
    const std::string& get_path() const { return path; }

    /// Returns the size of the file in bytes.
    std::size_t get_size() const { return text.size(); }

    /// Returns the line map for the file.
    const line_map& get_lines() const { return lines; }

    /// Returns the nth line in the file (starting from 0).
    std::string get_line(int n) const;

    /// Returns the 1-based line and column numbers for the location.
    coordinates resolve_location(location loc) const;

    // Iterators
    const char* begin() const { return text.c_str(); }
    const char* end() const { return begin() + text.size(); }

  private:
    /// The base offset for locations in this file.
    std::size_t offset;

    /// The path to the file, or empty if read from input or a string.
    ///
    /// FIXME: This should be a variant of some kind.
    std::string path;

    /// The text of the file.
    std::string text;

    /// An association of source locations with source lines.
    line_map lines;

    /// A list of line offsets.
    line_seq subs;
  };

} // namespace cc
