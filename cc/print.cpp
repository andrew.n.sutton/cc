#include "print.hpp"

#include <iostream>

namespace cc
{
  void
  printer::print(char c)
  {
    os << c;
  }
  
  void
  printer::print(const char* s)
  {
    os << s;
  }
  
  void
  printer::print(const std::string& s)
  {
    os << s;
  }
  
  void
  printer::print(symbol* s)
  {
    os << *s;
  }

  void
  printer::print(int n)
  {
    os << n;
  }

  void 
  printer::print(const void* p)
  {
    os << p;
  }

  printer::sexpr::sexpr(std::ostream& os, const char* str)
    : os(os)
  {
    os << '(' << str << ' ';
  }

  printer::sexpr::~sexpr()
  {
    os << ')';
  }

} // namespace cc