#include "diagnostics.hpp"
#include "input.hpp"
#include "output.hpp"

#include <cassert>
#include <iostream>
#include <fstream>

namespace cc
{
  const char*
  diagnostic::get_label() const
  {
    switch (dk) {
      case dk_error:
        return "error";
      case dk_warn:
        return "warning";
      case dk_note:
        return "note";
      case dk_info:
        return "info";
    }
  }

  void 
  diagnostic::note(location loc, const std::string& str)
  {
    notes.emplace_back(dk_note, loc, str);
  }

  void
  diagnostic_manager::emit(const diagnostic& diag)
  {
    assert(diag.get_kind() != dk_note && "emitting unattached note");
    diags.push_back(diag);
    if (diag.is_error())
      ++num_errs;
  }

  void
  diagnostic_manager::error(location loc, const std::string& msg)
  {
    diags.emplace_back(dk_error, loc, msg);
    ++num_errs;
  }

  void
  diagnostic_manager::error(const char* algo, location loc, const std::string& msg)
  {
    diags.emplace_back(dk_error, algo, loc, msg);
    ++num_errs;
  }

  void
  diagnostic_manager::warn(location loc, const std::string& msg)
  {
    diags.emplace_back(dk_warn, loc, msg);
  }

  void
  diagnostic_manager::warn(const char* algo, location loc, const std::string& msg)
  {
    diags.emplace_back(dk_warn, algo, loc, msg);
  }

  void
  diagnostic_manager::inform(location loc, const std::string& msg)
  {
    diags.emplace_back(dk_info, loc, msg);
  }    

  void
  diagnostic_manager::inform(const char* algo, location loc, const std::string& msg)
  {
    diags.emplace_back(dk_info, algo, loc, msg);
  }    
      
// -------------------------------------------------------------------------- //
// Serialization

  static const char*
  default_font()
  {
    return "\033[0m";
  }

  static const char*
  bold_font()
  {
    return "\033[1m";    
  }

  static void
  print_location(const diagnostic& diag, const input_manager& in, output_device& out)
  {
    std::ostream& os = out.get_stream();
    if (location loc = diag.get_location())
      os << bold_font() << in.resolve_location(loc) << default_font() << ": ";
  }

  static const char*
  label_font(diagnostic_kind k) 
  {
    switch (k) {
      case dk_error:
        return "\033[1;31m";
      case dk_warn:
        return "\033[1;35m";
      case dk_info:
        return "\033[1;36m";
      case dk_note:
        return "\033[1;34m";
    }
  }
  
  // Prints e.g., "error", possibly using color.
  static void
  print_label(const diagnostic& diag, output_device& out)
  {
    std::ostream& os = out.get_stream();
    if (out.use_color())
      os << label_font(diag.get_kind());
    if (const char* sub = diag.get_algorithm())
      os << sub << ' ';
    os << diag.get_label();
    if (out.use_color())
      os << default_font();
  }

  // Print the primary message.
  static void
  print_message(const diagnostic& diag, output_device& out)
  {
    std::ostream& os = out.get_stream();
    print_label(diag, out);
    os << ": " << diag.get_message() << '\n';
  }

  // Print the line (or lines?) surrounding the error. If the diagnostic is
  // locationless, we can't print the physical context.
  static void 
  print_context(const diagnostic& diag, const input_manager& in, output_device& out)
  {
    if (!diag.get_location())
      return;
    
    std::ostream& os = out.get_stream();
    full_location loc = in.resolve_location(diag.get_location());
    os << "  " << loc.source.get_line(loc.coords.line - 1) << '\n';    
    os << "  " << std::string(loc.coords.column - 1, ' ') << '^' << '\n';
  }

  void
  print(const diagnostic& diag, const input_manager& in, output_device& out)
  {
    print_location(diag, in, out);
    print_message(diag, out);
    print_context(diag, in, out);

    // Recursively emit extra diagnostics.
    //
    // TODO: In cases where we have an error with extensive diagnostics,
    // we might actually want additional inner formatting. Perhaps, provide
    // some additional grouping flags to help show that information.
    for (const diagnostic& d : diag.get_notes())
      print(d, in, out);
  }

  void
  print(const diagnostic_seq& diags, const input_manager& in, output_device& out)
  {
    for (const diagnostic& d : diags)
      print(d, in, out);
  }

} // namespace cc
