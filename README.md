A library of C++ facilities for compiler construction.

The goal of this project is to help simplify the process of creating compilers
by providing common and useful facilities. Currently, this project provides
facilities for:

- symbols and symbol tables,
- managing input files,
- representing source code locations,
- managing diagnostics,
- facilities for building abstract syntax trees,
- factories for managed node allocation,
- tools to support pretty printing,
- tools to support tree debugging (dumping)

This is very much a work in progress. It is not a stable project and subject
to fairly radical redesigns and restructurings.

Planned work includes additional facilities for:

- an extensible hashing model,
- string formatting facilities (probably through libfmt),
- an interface for colored terminal output,
- tools for binary serialization of abstract syntax trees,
- tools command line argument parsing.

